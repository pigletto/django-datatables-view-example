Example project for: https://bitbucket.org/pigletto/django-datatables-view

Installation:

1. Change to project directory
    
    ````
    $ cd django-datatables-view-example/
    ````
    
2. Create and activate virtualenv; install required packages: 

    ````
    $ virtualenv env --python=python3
    $ source env/bin/activate
    $ pip install -r requirements.txt
    ````
   
    or with pipenv

    ````
    $ pipenv install
    $ pipenv shell
    ````

3. Create database
    ```` 
    ./manage.py migrate
    ````

4. Create superuser to have database filled with some data
    ```` 
    ./manage.py createsuperuser
    ````

5. Run
    ````
    ./manage.py runserver
    ````

Visit http://localhost:8000

To add some records to database go to: http://localhost:8000/admin/ and add new users.
