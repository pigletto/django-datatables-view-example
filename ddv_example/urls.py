from django.urls import re_path

from .views import UsersList, UsersListJson, IndexView, UsersList110, UsersList110Json, TestModelList, TestModelListJson

urlpatterns = [
    re_path(r'^$', IndexView.as_view(), name="index"),
    re_path(r'^datatables_19$', UsersList.as_view(), name="datatables_19"),
    re_path(r'^users_data_19/$', UsersListJson.as_view(), name="users_list_json_19"),

    re_path(r'^datatables_110$', UsersList110.as_view(), name="datatables_110"),
    re_path(r'^users_data_110/$', UsersList110Json.as_view(), name="users_list_json_110"),

    re_path(r'^testmodel$', TestModelList.as_view(), name="testmodel"),
    re_path(r'^testmodel_data/$', TestModelListJson.as_view(), name="testmodel_list_json"),
]
