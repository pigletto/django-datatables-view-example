from django.apps import AppConfig


class DdtConfig(AppConfig):
    name = 'ddv_example'
    verbose_name = 'Django Datatables View Example'
